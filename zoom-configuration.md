# Zoom Client Configuration

## Installation
Unless, you already have it, install the Zoom client

* [Zoom Client for Meetings](https://us02web.zoom.us/download)

## Configuration
Start the client and open the *Settings* window.

![](img/zoom-1.png)

Choose, one at a time, the tabs *Video* and *Audio*
respectively.

![](img/zoom-2.png)

Verify that you can see yourself in the *Video settings*.

![](img/zoom-3.png)

Verify that you hear the test sound (*Test Speaker*) and
can record your voice (*Test Mic*) in the *Audio settings*.

![](img/zoom-4.png)

## Registration
Don't forget to register yourself in Zoom using the
provided link in My-Ribomation.


