# Linux and C++

Although, there are many ways to write and compile C++ programs both on Windows,
Linux and Mac, our experience of giving many courses on various levels; is that
using Ubuntu Linux provide the least amount of surprises and distracting
technical struggles.

That means for all our C/C++ courses, we do recommend the following setup:
* Ubuntu Linux, version 20.04 (or later)
* GCC C++ compiler, version 11 (or later)
* [JetBrains CLion IDE, 30-days trial](https://www.jetbrains.com/clion/download/)


# Linux or similar
If you already have a computer running Linux, you probably are good to go.
If it's not Ubuntu you might have to adjust the installation commands below.

If you're using a Mac, you're also good to go.

If your computer runs *Windows 10/11*, you can install Linux on top of WSL.
Just follow the instructions below. 

Otherwise, you can install a virtual VM
software, such as *VirtualBox* or *VMware* and install Linux to a virtual machine.
Check the instructions for VirtualBox below.


## Ubuntu Linux @ WSL (Windows 10/11)
One of the biggest news of Windows 10, is that it provides the _Windows Subsystem for Linux_ (WSL) plugin. 
Follow the installation instructions by Microsoft.
* [How to Install WSL 2 on Windows 10](https://www.omgubuntu.co.uk/how-to-install-wsl2-on-windows-10)
* [WSL Installation Guide](https://docs.microsoft.com/en-us/windows/wsl/install-win10)

Then open _Microsoft Store_ and search for "Ubuntu" and choose
Ubuntu 22.04 and install it. It's as simple as that.

Our favorite C++ IDE is CLion and it works very nicely with WSL; i.e. you install
_CLion on Windows_ and uses the compiler resources within WSL over SSH. Just follow the 
instructions at
* [How to Use WSL Development Environment in CLion](https://www.jetbrains.com/help/clion/how-to-use-wsl-development-environment-in-clion.html)
* [Using WSL toolchains in CLion on Windows (YouTube)](https://youtu.be/xnwoCuHeHuY)


## Ubuntu Linux @ VirtualBox
If you're running an earlier version of Windows, then install VirtualBox, create a
virtual machine for Ubuntu and download/install Ubuntu to it.

1. Install VirtualBox (VBox)<br/>
    <https://www.virtualbox.org/wiki/Downloads>
1. Create a new virtual machine (VM) i VBox, for Ubuntu Linux<br/>
    <https://www.virtualbox.org/manual/ch03.html>
1. Download an ISO file for the latest version of Ubuntu Desktop 64-bit<br/>
    <http://www.ubuntu.com/download/desktop>
1. Choose as much RAM for the VM as you can, still within the "green" region.
1. Choose as much video memory you can
1. Create an auto-expanding virtual hard-drive of 50GB
1. Mount the ISO file in the virtual CD drive of your VM
1. Start the VM and run the Ubuntu installation program.
    Ensure you install to the (virtual) hard-drive.
1. Set a username and password when asked to and write them down so you remember them.
   Also, go for auto-logon.
1. Install the VBox guest additions<br/>
    <https://www.virtualbox.org/manual/ch04.html>
1. Install Clion from the Ubuntu programs installer app
    Within Ubuntu, open the program listing app and search for JetBrains CLion, then install it.


# GCC C++ Compiler and Tools
Within a Ubuntu terminal window type the following command to install the
compiler and other tools.

    sudo apt install g++-11 gdb make cmake valgrind

_N.B._ when you run a `sudo` command it prompts you for the password, you use
to logon to Ubuntu. If you're running another OS, amend the installtion command
accordingly.

Verify the version

    gcc-11 --version
    g++-11 --version


## How to change the default GCC version

When you install a particular version of gcc it might be that the linux
system has another (lower) version as its standard. To change the default
you use the `update-alternatives` command (*one long command-line below*)

    sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-11 90 --slave /usr/bin/g++ g++ /usr/bin/g++-11 --slave /usr/bin/gcov gcov /usr/bin/gcov-11

Verify that GCC version 11, now is the default

    gcc --version
    g++ --version
